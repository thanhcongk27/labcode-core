package com.lip6.gpstl.dao;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.lip6.gpstl.model.Folder;
import com.lip6.gpstl.model.Project;
import com.lip6.gpstl.model.User;

/**
 * A simple test class for DAO.
 * 
 * @author Ryme
 * 
 */
public class MainDaoTest {
	static final Logger logger = Logger.getLogger(MainDaoTest.class);

	// static Object getSpringBean(String name) {
	// ApplicationContext context = new ClassPathXmlApplicationContext(
	// new String[] {
	// //
	// "applicationContext-resources.xml" //
	// , "applicationContext-dao.xml" //
	// // , "applicationContext-service.xml" //
	// , "applicationContext.xml" //
	// });
	//
	// Object ret = context.getBean(name);
	// return ret;
	// }

	static void listUsers() {
		UserDao daoUser = DAOFactory.getUserDao();
		daoUser.beginTransaction();
		try {
			List<User> users = daoUser.getUsers();
			System.out.println("users=" + users);
		} finally {
			daoUser.closeTransaction();
		}
	}

	static void createProject() {

		Project prj = new Project();

		prj.setIdAuthor(-3); // attribution du projet a admin
		prj.setDepositDate(new Date()); // Now
		prj.setProjectDescription("Test project post by admin.");
		prj.setProjectKeyWords("Project #" + (System.currentTimeMillis()));
		prj.setProjectName("Test #" + (System.currentTimeMillis()));
		prj.setProjectRight("ALL");

		ProjectDao daoProject = DAOFactory.getProjectDao();
		daoProject.beginTransaction();
		try {
			daoProject.createProject(prj);
			// COpier sur le serveur
			
			daoProject.commit();
		} catch (Exception ex) {
			daoProject.cancelTransaction();
			// Serveur.delete
		} finally {
			daoProject.closeTransaction();
		}
		System.out
				.println("MainDaoTest.createProject()::: FIN CREATION PROJET");
	}


	static void createFolder() {

		Folder dir = new Folder();
		dir.setIdProject(1); // Associe ce folder au projet numero 1.
		dir.setFolderName("folder1");

		FolderDao daoFolder = DAOFactory.getFolderDao();
		daoFolder.beginTransaction();
		try {
			daoFolder.save(dir);
			daoFolder.commit();
		} finally {
			daoFolder.closeTransaction();
		}
		System.out.println("MainDaoTest.createFolder()::: Fin creation folder.");
	}

	
	public static void main(String[] args) {
		try {
			System.out.println("==========================================");
			System.out.println("A simple debug test for DAO");
			System.out.println("==========================================");

			listUsers();
			createProject();
			createFolder();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		System.out.println("==========================================");
		System.out.println("FIN");
		System.out.println("==========================================");

		System.exit(0);
	}
}
