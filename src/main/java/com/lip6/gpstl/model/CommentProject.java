package com.lip6.gpstl.model;

// Generated Nov 11, 2014 12:23:55 AM by Hibernate Tools 4.0.0

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * CommentProject generated by hbm2java
 */
@Entity
@Table(name = "comment_project", catalog = "labcode_db")
public class CommentProject implements java.io.Serializable {

	private CommentProjectId id;
	private String commentContent;

	public CommentProject() {
	}

	public CommentProject(CommentProjectId id, String commentContent) {
		this.id = id;
		this.commentContent = commentContent;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "idComment", column = @Column(name = "id_comment", nullable = false)),
			@AttributeOverride(name = "idUser", column = @Column(name = "id_user", nullable = false)),
			@AttributeOverride(name = "idProject", column = @Column(name = "id_project", nullable = false)) })
	public CommentProjectId getId() {
		return this.id;
	}

	public void setId(CommentProjectId id) {
		this.id = id;
	}

	@Column(name = "comment_content", nullable = false, length = 20000)
	public String getCommentContent() {
		return this.commentContent;
	}

	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}

}
