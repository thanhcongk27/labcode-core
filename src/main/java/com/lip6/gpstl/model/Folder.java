package com.lip6.gpstl.model;

// Generated Nov 11, 2014 12:23:55 AM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Folder generated by hbm2java
 */
@Entity
@Table(name = "Folder", catalog = "labcode_db")
public class Folder implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idFolder;
	private int idProject;
	private Integer idParentFolder;
	private String folderName;

	public Folder() {
	}

	public Folder(int idProject) {
		this.idProject = idProject;
	}

	public Folder(int idProject, Integer idParentFolder) {
		this.idProject = idProject;
		this.idParentFolder = idParentFolder;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_Folder", unique = true, nullable = false)
	public Integer getIdFolder() {
		return this.idFolder;
	}

	public void setIdFolder(Integer idFolder) {
		this.idFolder = idFolder;
	}

	@Column(name = "id_project", nullable = false)
	public int getIdProject() {
		return this.idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}

	@Column(name = "id_parent_Folder")
	public Integer getIdParentFolder() {
		return this.idParentFolder;
	}

	public void setIdParentFolder(Integer idParentFolder) {
		this.idParentFolder = idParentFolder;
	}
	
	@Column(name = "folder_name", nullable = false, length = 20)
	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	
	

}
