package com.lip6.gpstl.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DAOFactory {
	private static ApplicationContext context;

	private static Object getSpringBean(String name) {
		if (context == null) {
			context = new ClassPathXmlApplicationContext(new String[] {
					//
					"applicationContext-resources.xml" //
					, "applicationContext-dao.xml" //
					// , "applicationContext-service.xml" //
					, "applicationContext.xml" //
			});
		}
		Object ret = context.getBean(name);
		return ret;
	}

	public static UserDao getUserDao() {
		return (UserDao) getSpringBean("daoUser");
	}

	public static ProjectDao getProjectDao() {
		return (ProjectDao) getSpringBean("daoProject");
	}

	public static FolderDao getFolderDao() {
		return (FolderDao) getSpringBean("daoFolder");
	}

	public static FileDao getFileDao() {
		return (FileDao) getSpringBean("daoFile");
	}

}