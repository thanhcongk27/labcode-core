package com.lip6.gpstl.dao;

import com.lip6.gpstl.model.Folder;
/**
 * Repository Data Access Object Interface
 * 
 * @author
 *
 */

public interface FolderDao extends GenericDao<Folder, Integer>{

	/**
	 * create a repository in a project and insert it in the "Repository" table
	 * @param repository
	 */
	void createFolder(Folder folder);

	
}
