package com.lip6.gpstl.dao;

import com.lip6.gpstl.model.Project;

/**
 * Project Data Access Object Interface
 * 
 * @author Ryme Fantazi
 * 
 */

public interface ProjectDao extends GenericDao<Project, Integer> {

	/**
	 * insert a new project into the database
	 * @param project
	 */
	void createProject(Project project);

	/**
	 * update the project 
	 * @param project
	 */
	void updateProject(Project project);
}
