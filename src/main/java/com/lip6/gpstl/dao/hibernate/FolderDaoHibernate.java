package com.lip6.gpstl.dao.hibernate;

import com.lip6.gpstl.dao.FolderDao;
import com.lip6.gpstl.model.Folder;
/**
 * 
 * @author Ryme Fantazi
 *
 */
public class FolderDaoHibernate extends GenericDaoHibernate<Folder, Integer> implements FolderDao{

	/**
	 * 
	 */
	
	public FolderDaoHibernate() {
		super(Folder.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createFolder(Folder folder) {
		// TODO Auto-generated method stub
		if (log.isDebugEnabled()) {
            log.debug("Folder = " + folder);
        }
        getSession().saveOrUpdate(folder);
        // necessary to throw a DataIntegrityViolation and catch it in FolderManager
        getSession().flush();
		
	}

}
