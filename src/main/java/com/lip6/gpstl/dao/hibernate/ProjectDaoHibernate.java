package com.lip6.gpstl.dao.hibernate;

import com.lip6.gpstl.dao.ProjectDao;
import com.lip6.gpstl.model.Project;

public class ProjectDaoHibernate extends GenericDaoHibernate<Project, Integer> implements ProjectDao {

	
	public ProjectDaoHibernate() {
		super(Project.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createProject(Project project) {
		// TODO Auto-generated method stub
		if (log.isDebugEnabled()) {
            log.debug("project = " + project);
        }
        getSession().saveOrUpdate(project);
        // necessary to throw a DataIntegrityViolation and catch it in ProjectManager
        getSession().flush();
		
	}

	@Override
	public void updateProject(Project project) {
		// TODO Auto-generated method stub
		
	}

}
