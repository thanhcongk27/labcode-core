package com.lip6.gpstl.dao;

/**
 *
 * @author jgarcia
 */
public class SearchException extends RuntimeException {

    public SearchException(Throwable ex) {
        super(ex);
    }

}
