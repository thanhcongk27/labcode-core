package com.lip6.gpstl.dao;

import java.awt.List;

import com.lip6.gpstl.model.File;
/**
 * File Data Access Object Interface
 * 
 * @author Ryme Fantazi
 *
 */

public interface FileDao extends GenericDao<File, Integer>{
	/**
	 * create a file in a repository and insert it in the 'File' table 
	 * @param repository
	 */
	
	
	public void addFile(File file);
	public File getFile(Integer fileId);
	public List getAllFiles();
}
