/* etape 1:  creer utilisateur labcode_user*/
/*
CREATE USER `labcode_user`@`localhost` IDENTIFIED BY `123456`;
GRANT ALL PRIVILEGES ON * . * TO `labcode_user`@`localhost`;
FLUSH PRIVILEGES;
*/


/* etape 2: creer une base de donnees labcode_db*/
/*
CREATE DATABASE IF NOT EXISTS labcode_db CHARACTER SET `utf8`;
USE labcode_db;
*/


/** etape 3: creer des tables et importer des donnees
 mysql -u labcode_user -p;
 use labcode_db;
 source /chemin.../LCR_database.sql
 * 
 * */

-- ----------------------------------------------------------------------------------------------------- --
-- IMPORTANT ::: Ordre de suppression des tables a cause des contraintes de clés étrangères! :           --
-- On commence d'abord par les tables dépendantes, puis les dépendances (ordre inverse de la création).  --
-- ----------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS `comment_file`;
DROP TABLE IF EXISTS `comment_project`;
DROP TABLE IF EXISTS `file`;
DROP TABLE IF EXISTS `folder`;
DROP TABLE IF EXISTS `project`;
DROP TABLE IF EXISTS `user_role`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `app_user`;

-- FIN SUPPRESION DES TABLES --





-- Creation de la table app_user :
CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_expired` bit(1) NOT NULL,
  `account_locked` bit(1) NOT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postal_code` varchar(15) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `credentials_expired` bit(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `account_enabled` bit(1) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_hint` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
   `secret_question` varchar(50) NOT NULL,
   `secret_response` varchar(50) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ;

-- LOCK TABLES `app_user` WRITE;

INSERT INTO `app_user` 
(  
	`id`, `account_expired`, `account_locked`, `address`, `city` 
  	, `country`, `postal_code`, `province`, `credentials_expired`, `email` 
  	, `account_enabled`, `first_name`, `last_name`, `password`, `password_hint` 
  	, `phone_number`, `username`, `secret_question`, `secret_response`, `version` 
  	, `website`
 ) VALUES 
 (	-3,'\0','\0','', 'Denver'
 	,'US','80210','CO','\0','two_roles_user@appfuse.org'
 	,'','Two Roles','User','$2a$10$bH/ssqW8OhkTlIso9/yakubYODUOmh.6m5HEJvcBq3t3VdBh7ebqO','Not a female kitty.'
 	,'', 'two_roles_user', '' , '', 1
 	,'http://raibledesigns.com'),
(	-2,'\0','\0','','Denver'
	,'US','80210','CO','\0','matt@raibledesigns.com'
	,'','Matt','Raible','$2a$10$bH/ssqW8OhkTlIso9/yakubYODUOmh.6m5HEJvcBq3t3VdBh7ebqO','Not a female kitty.'
	,'', 'admin', '' , '', 1,
	'http://raibledesigns.com'),
(	-1,'\0','\0','','Denver'
	,'US','80210','CO','\0','matt_raible@yahoo.com'
	,'','Tomcat','User','$2a$10$CnQVJ9bsWBjMpeSKrrdDEeuIptZxXrwtI6CZ/OgtNxhIgpKxXeT9y','A male kitty.'
	,'','user', '' , '', 1
	,'http://tomcat.apache.org');

-- UNLOCK TABLES;


-- Creation de la tables des roles :
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;


INSERT INTO `role` (id, description, name) 
VALUES 
	  (-2,'Default role for all Users','ROLE_USER')
	, (-1,'Administrator role (can edit Users)','ROLE_ADMIN');

--
-- Table structure for table `user_role`
--

-- Creation de la table des droits utilisateur :
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`), 
  FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
); 

-- LOCK TABLES `user_role` WRITE;

-- INSERT INTO `user_role` VALUES (-3,-2),(-1,-2),(-3,-1),(-2,-1);

-- UNLOCK TABLES;


-- Creation de la table des projets :
CREATE TABLE `project` (
	`id_project` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`project_name` VARCHAR(20) NOT NULL,
	`project_description` TEXT NOT NULL,
	`id_author` bigint(20) NOT NULL,
	`deposit_date` DATE NOT NULL DEFAULT '0000-00-00',
	`project_version` INT UNSIGNED,
	`project_key_words` VARCHAR(255) NOT NULL,
	`project_right` VARCHAR(10) NOT NULL,
	`number_views` INT UNSIGNED,
	`number_downloads` INT UNSIGNED,
	PRIMARY KEY (`id_project`)
	, FOREIGN KEY (`id_author`) REFERENCES `app_user` (`id`) ON DELETE CASCADE
);



-- Creation de la table des répertoires :
CREATE TABLE `folder` (
	`id_folder` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_project` INT UNSIGNED NOT NULL,
	`id_parent_folder` INT UNSIGNED,
	`folder_name` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`id_folder`),
	FOREIGN KEY (`id_project`) REFERENCES `project`(`id_project`) ,
	FOREIGN KEY (`id_parent_folder`) REFERENCES `folder`(`id_folder`)
);


-- Creation de la table des fichiers :
CREATE TABLE `file` (
	`id_file` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`file_name` VARCHAR(20) NOT NULL,
	`file_type` VARCHAR(20) NOT NULL,
	`file_url` TEXT NOT NULL,
	`file_extension` VARCHAR(20) NOT NULL,
	`id_folder` INT UNSIGNED,
	`id_project` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`id_file`),
	FOREIGN KEY (`id_folder`) REFERENCES `folder`(`id_folder`),
	FOREIGN KEY (`id_project`) REFERENCES `project`(`id_project`)
);

-- Creation de la table de commentaire des fichiers :
CREATE TABLE `comment_file` (
	`id_comment` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_user` bigint(20) NOT NULL,
	`id_file` INT UNSIGNED NOT NULL,
	`comment_content` TEXT NOT NULL,
	PRIMARY KEY (id_comment,id_user,id_file),
	FOREIGN KEY (id_user) REFERENCES `app_user`(`id`),
	FOREIGN KEY (`id_file`) REFERENCES `file`(`id_file`)
);

-- Creation de la table de commentaire des projets :
CREATE TABLE `comment_project` (
	`id_comment` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_user` bigint(20) NOT NULL,
	`id_project` INT UNSIGNED NOT NULL,
	`comment_content` TEXT NOT NULL,
	PRIMARY KEY (`id_comment`,`id_user`,`id_project`),
	FOREIGN KEY (`id_user`) REFERENCES `app_user`(`id`),
	FOREIGN KEY (`id_project`) REFERENCES `project`(`id_project`) 
);

